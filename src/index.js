import fs from 'fs';
import request from 'request';
import promiseAdapter from 'promise-adapter';

export async function get(url, needBufferBody = false, opts = {}) {
    const options = {
        url: url,
        method: 'GET'
    };

    Object.assign(options, opts);

    if (needBufferBody) {
        options.encoding = null;
    }

    const result = await promiseAdapter(request)(options);
    return {
        statusCode: result[0].statusCode,
        body: result[1],
        respond: result[0]
    };
}

export async function getJSON(url, opts = {}) {
    let result = await get(url, false, opts);
    if (result.statusCode === 200) {
        result.body = JSON.parse(result.body);
    }
    return result;
}

export async function postJSON(url, json, needBufferBody = false, opts = {}) {

    const options = {
        url: url,
        method: 'POST',
        json: json,
        headers: {
            'content-type': 'application/json'
        }
    };

    Object.assign(options, opts);

    if (needBufferBody) {
        options.encoding = null;
    }

    const result = await promiseAdapter(request)(options);
    return {
        statusCode: result[0].statusCode,
        body: result[1],
        respond: result[0]
    };
}

export async function postXML(url, xml, needBufferBody = false, opts = {}) {

    const options = {
        url: url,
        method: 'POST',
        body: xml,
        headers: {
            'content-type': 'application/xml'
        }
    };

    Object.assign(options, opts);

    if (needBufferBody) {
        options.encoding = null;
    }

    const result = await promiseAdapter(request)(options);
    return {
        statusCode: result[0].statusCode,
        body: result[1],
        respond: result[0]
    };
}

export async function postBinary(url, buffer, needBufferBody = false, opts = {}) {

    const options = {
        url: url,
        method: 'POST',
        body: buffer,
        headers: {
            'content-type': 'application/binary'
        }
    };

    Object.assign(options, opts);

    if (needBufferBody) {
        options.encoding = null;
    }

    const result = await promiseAdapter(request)(options);
    return {
        statusCode: result[0].statusCode,
        body: result[1],
        respond: result[0]
    };
}

export async function download(url, path, opts = {}) {

    const options = {
        url: url,
        method: 'GET'
    };

    Object.assign(options, opts);

    await promiseAdapter((callback) => {
        request(options).pipe(fs.createWriteStream(path).on('error', (e) => {
            callback(e);
        }).on('finish', () => {
            callback(null);
        }));
    })();
    return;
}
